import React,{useEffect} from 'react';
import logo from './logo.svg';
import './App.css';
import {BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Index from './pages';
import About from './pages/about';
import AllBlogs from './pages/AllBlogs';
import ContractDetails from './pages/contractDetails';
import PrevResult from './pages/prevresult';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap'
import 'bootstrap/dist/js/bootstrap.bundle'
import LatestWinner from './pages/latestwinners';
import Faq from './pages/faq';
import Error404 from './pages/error';
import ContactUs from './pages/contactus';
import AllLotteries from './pages/AllLotteries';
function App() {
  return (
    <Router>
    <Switch>
      <Route path="/" exact component={Index} />
      <Route path="/about" exact component={About} />
      <Route path="/allblogs" exact component={AllBlogs} />
      <Route path="/contractdetails/:id" exact component={ContractDetails} />
      <Route path="/prevresult" exact component={PrevResult} />
      <Route path="/latestwinner" exact component={LatestWinner} /> 
      <Route path="/faq" exact component={Faq} />  
      <Route path="/contactus" exact component={ContactUs} />
      <Route path="/alllotteries" exact component={AllLotteries} />  
      <Route component={Error404} /> 
      </Switch>       
    </Router>
  );
}

export default App;
