import React,{useState} from 'react'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter,Input} from 'reactstrap';
import ModalInput from './modalinput';
const CreateButton=()=>{
    const [isOpen,setisOpen]=useState(false)
    const [isloading,setisloading]=useState(true)
    const showModal=()=>{
        setisOpen(!isOpen)
    }


    if(isOpen)
    {
        return(
            <Modal isOpen={isOpen} toggle={showModal}>
            <ModalHeader toggle={showModal}>Modal title</ModalHeader>
            <ModalBody>
            <ModalInput/>
            </ModalBody>
            <ModalFooter>
            <Button color="primary" onClick={showModal}>Cancel</Button>
            </ModalFooter>
            </Modal>
        );
    }
    return(
        <div style={{display:'flex',justifyContent:'center'}}>
            <div className="btn-area" style={{alignSelf:'center',cursor:'pointer'}} onClick={()=>{
                showModal()
            }}>
                <a className="cmn-btn" style={{textDecoration:'none',color:'white'}}>Create Your Own Lottery</a>
            </div>
        </div>
    );
}

export default CreateButton;