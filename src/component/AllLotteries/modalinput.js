import React,{useState} from 'react'
import {fileUpload} from '../../actions/index';
const ModalInput=(props)=>{
    const [Name,setName]=useState("")
    const [date,setDate]=useState("")
    const [file,setFile]=useState({})
    const [errormessage,setError]=useState("")
    const submitInfo=async()=>
    {
        let image=""
        if(Name!=="" && date!=="" && file!=={})
        {
            let response=await fileUpload(file)
            if(response===null)
            {
                setError("something went wrong in uploading the file")
            }else{
                image=response.location
            }
            console.log("Uploaded the file")
        }else
        {
            setError("All fields are required")
        }
    }

    const filehandler=(e)=>{
        setFile(e.target.files[0])
    }
    const removeError=()=>{
        setError("")
    }
    return(
        <div className="contact-form">
            <div className="form-grp">
            <input type="text" name="contact_name" id="contact_name" placeholder="Name of Lottery"
                onBlur={(e)=>{
                    setName(e.target.value)
                }}
                onFocus={removeError}
            />
            </div>
            <div className="form-grp">
            <input type='date' className='contact_name' onChange={(e)=>{
                setDate(e.target.value)
            }}
            onFocus={removeError}
            />
            </div>
            <h5 style={{padding:10}}>Give the avatar for the particular Lottery</h5>
            <div className="upload-btn-wrapper">
            <button className="btn-upload-button">Upload a file</button>
            <input type="file" name="myfile" onChange={filehandler} 
                onFocus={removeError}
            />
            </div>
            <p style={{color:"red",padding:10,fontSize:18}}>{errormessage}</p>
            <div className="form-grp">
                <input className="submit-btn" type="submit" onClick={()=>{
                    submitInfo()
                }} value="Enter Lottery"/>
            </div>

        </div>
    );
}

export default ModalInput;