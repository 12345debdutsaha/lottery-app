import React from 'react'

const BuyTicketsOnline=()=>{
    return(
        <section className="online-ticket-section section-padding has_bg_image">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-lg-8">
            <div className="section-header text-center">
              <h2 className="section-title">Buy Lottery Tickets Online</h2>
              <p>Play the lottery online safely and securely at theLotter, the leading lottery ticket purchasing service in the world </p>
            </div>
          </div>
        </div>
        <div className="row justify-content-center">
          <div className="col-lg-10">
            <div className="online-ticket-table-part">
              <div className="online-ticket-table">
                <h3 className="block-title">Select your winning lottery numbers</h3>
                <div className="online-ticket-table-wrapper">
                  <table>
                    <thead>
                      <tr>
                        <th className="name">lottery</th>
                        <th className="jackpot">jackpot</th>
                        <th className="price">price</th>
                        <th className="draw-time">time to draw</th>
                        <th className="sold-num">sold</th>
                        <th className="status">status</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          <div className="winner-details"><img src="assets/images/flag/1.jpg" /><span className="winner-name">cancer charity</span></div>
                        </td>
                        <td>
                          <span className="jackpot-price">€ 53,000,000</span>
                        </td>
                        <td>
                          <span className="price">€3.9</span>
                        </td>
                        <td>
                          <div className="draw-timer"></div>
                        </td>
                        <td>
                          <div className="progressbar" data-perc="50%">
                            <div className="bar"></div>
                            <span className="label">50</span>
                          </div>
                        </td>
                        <td>
                          <a href="#" className="cmn-btn">buy ticket</a>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div className="winner-details"><img src="assets/images/flag/2.jpg" /><span className="winner-name">US Powerball</span></div>
                        </td>
                        <td>
                          <span className="jackpot-price">€ 53,000,000</span>
                        </td>
                        <td>
                          <span className="price">€3.9</span>
                        </td>
                        <td>
                          <div className="draw-timer"></div>
                        </td>
                        <td>
                          <div className="progressbar" data-perc="50%">
                            <div className="bar"></div>
                            <span className="label">50</span>
                          </div>
                        </td>
                        <td>
                          <a href="#" className="cmn-btn">buy ticket</a>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div className="winner-details"><img src="assets/images/flag/3.jpg" /><span className="winner-name">Mega Millions</span></div>
                        </td>
                        <td>
                          <span className="jackpot-price">€ 53,000,000</span>
                        </td>
                        <td>
                          <span className="price">€3.9</span>
                        </td>
                        <td>
                          <div className="draw-timer"></div>
                        </td>
                        <td>
                          <div className="progressbar" data-perc="50%">
                            <div className="bar"></div>
                            <span className="label">50</span>
                          </div>
                        </td>
                        <td>
                          <a href="#" className="cmn-btn">buy ticket</a>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div className="winner-details"><img src="assets/images/flag/1.jpg"/><span className="winner-name">UK Lotto</span></div>
                        </td>
                        <td>
                          <span className="jackpot-price">€ 53,000,000</span>
                        </td>
                        <td>
                          <span className="price">€3.9</span>
                        </td>
                        <td>
                          <div className="draw-timer"></div>
                        </td>
                        <td>
                          <div className="progressbar" data-perc="50%">
                            <div className="bar"></div>
                            <span className="label">50</span>
                          </div>
                        </td>
                        <td>
                          <a href="#" className="cmn-btn">buy ticket</a>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <div className="winner-details"><img src="assets/images/flag/3.jpg"/><span className="winner-name">Mega Millions</span></div>
                        </td>
                        <td>
                          <span className="jackpot-price">€ 53,000,000</span>
                        </td>
                        <td>
                          <span className="price">€3.9</span>
                        </td>
                        <td>
                          <div className="draw-timer"></div>
                        </td>
                        <td>
                          <div className="progressbar" data-perc="50%">
                            <div className="bar"></div>
                            <span className="label">50</span>
                          </div>
                        </td>
                        <td>
                          <a href="#" className="cmn-btn">buy ticket</a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-12 text-center">
            <a href="#" className="text-btn">view all lotteries</a>
          </div>
        </div>
      </div>
    </section>
    );
}
export default BuyTicketsOnline;