import React from 'react'

const PaymentMethods=()=>{
    return(
        <section className="payment-method-section section-padding border-top">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-lg-6">
            <div className="section-header text-center">
              <h2 className="section-title">Payment Method</h2>
              <p>Buy international lottery tickets online using any of the payment methods available on Sorteo Play now and win big!</p>
            </div>
          </div>
        </div>
        <div className="row justify-content-center">
          <div className="col-lg-10">
            <div className="payment-method-area d-flex">
              <div className="payment-item">
                <a href="#0"><img src="assets/images/payment-methods/1.jpg"/></a>
              </div>
              <div className="payment-item">
                <a href="#0"><img src="assets/images/payment-methods/2.jpg"/></a>
              </div>
              <div className="payment-item">
                <a href="#0"><img src="assets/images/payment-methods/3.jpg"/></a>
              </div>
              <div className="payment-item">
                <a href="#0"><img src="assets/images/payment-methods/4.jpg"/></a>
              </div>
              <div className="payment-item">
                <a href="#0"><img src="assets/images/payment-methods/5.jpg"/></a>
              </div>
              <div className="payment-item">
                <a href="#0"><img src="assets/images/payment-methods/6.jpg"/></a>
              </div>
            </div>
          </div>
          <div className="col-lg-12">
            <div className="commission-area d-flex align-items-center">
              <div className="title-area">
                <h3 className="title">Referral commission</h3>
              </div>
              <div className="commission-items-wrapper d-flex align-items-center">
                <div className="commission-items d-flex align-items-center">
                  <div className="icon">
                    <img src="assets/images/payment-methods/cm1.png"/>
                  </div>
                  <div className="content">
                    <span className="percentage">10%</span>
                    <p>1st Level</p>
                  </div>
                </div>
                <div className="commission-items d-flex align-items-center">
                  <div className="icon">
                    <img src="assets/images/payment-methods/cm2.png"/>
                  </div>
                  <div className="content">
                    <span className="percentage">05%</span>
                    <p>2nd Level</p>
                  </div>
                </div>
                <div className="commission-items d-flex align-items-center">
                  <div className="icon">
                    <img src="assets/images/payment-methods/cm3.png"/>
                  </div>
                  <div className="content">
                    <span className="percentage">03%</span>
                    <p>3rd Level</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    );
}
export default PaymentMethods;