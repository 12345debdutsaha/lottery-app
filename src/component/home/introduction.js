import React from 'react'

const Introduction=()=>{
    return(
        <section className="banner-section">
      <div className="banner-elements-part has_bg_image" data-background="assets/images/elements/vector-bg.jpg">
          <div className="element-one"><img src="assets/images/elements/box.png" /></div>

          <div className="element-two"><img src="assets/images/elements/car.png" /></div>

          <div className="element-three"><img src="assets/images/elements/chart.png" /></div>

          <div className="element-four"><img src="assets/images/elements/dollars.png" /></div>

          <div className="element-five"><img src="assets/images/elements/laptop.png" /></div>

          <div className="element-six"><img src="assets/images/elements/money-2.png" /></div>

          <div className="element-seven"><img src="assets/images/elements/person.png" /></div>

          <div className="element-eight"><img src="assets/images/elements/person-2.png" /></div>

          <div className="element-nine"><img src="assets/images/elements/power.png" /></div>
      </div>
      <div className="banner-content-area">
          <div className="container">
              <div className="row">
                  <div className="col-md-6">
                      <div className="banner-content">
                          <h1 className="title">Take the chance to change your life</h1>
                          <p>Sorteo is online lottery platform inspired by few sorteo lover's fantasy of the ultimate lottery platfrom.</p>
                          <a href="#0" className="cmn-btn">buy ticket now !</a>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </section>
    );
}

export default Introduction;