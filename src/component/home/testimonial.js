import React from 'react'

const Testimonial=()=>{
    return(
        <section className="testimonial-section section-padding border-top">
      <div className="container">
        <div className="row justify-content-between">
          <div className="col-lg-6">
            <div className="testimonial-content">
              <h5 className="sub-title">hat they think about us</h5>
              <h2 className="title">Testimonials</h2>
              <div className="total-ratings">
                <i className="fa fa-star"></i>
                <i className="fa fa-star"></i>
                <i className="fa fa-star"></i>
                <i className="fa fa-star"></i>
                <i className="fa fa-star"></i>
                <span className="ratings-count-num">3.4M Reviews</span>
              </div>
              <p>With over 12 years of experience as the world’s leading online lottery service provider, Sorteo has catered to over a million users. Find out what our members have to say about us! </p>
              <div className="testimonial-slider-arrows d-flex">
                <div className="button-next"><i className="fa fa-angle-left" aria-hidden="true"></i></div>
                <div className="button-prev"><i className="fa fa-angle-right" aria-hidden="true"></i></div>
              </div>
            </div>
          </div>
          <div className="col-lg-5">
            <div className="testimonial-slider swiper-container">
              <div className="swiper-wrapper">
                <div className="swiper-slide">
                  <div className="testimonial-slide">
                    <div className="testimonial-slide-header d-flex">
                      <div className="client-thumb">
                        <img src="assets/images/testimonial/1.png" />
                      </div>
                      <div className="client-details">
                        <h5 className="name">Albert G.</h5>
                        <div className="ratings">
                          <i className="fa fa-star"></i>
                          <i className="fa fa-star"></i>
                          <i className="fa fa-star"></i>
                          <i className="fa fa-star"></i>
                          <i className="fa fa-star"></i>
                          <span>5.00</span>
                        </div>
                        <p><span className="place">France</span><span className="date">15th June, 2018</span></p>
                      </div>
                    </div>
                    <div className="testimonial-slide-body">
                      <p>I've played with Sorteo for several years and really appreciate the site. All of my wins have been credited to my account. Thanks to the entire team at  Sorteo!"</p>
                    </div>
                  </div>
                </div>
                <div className="swiper-slide">
                  <div className="testimonial-slide">
                    <div className="testimonial-slide-header d-flex">
                      <div className="client-thumb">
                        <img src="assets/images/testimonial/2.png"/>
                      </div>
                      <div className="client-details">
                        <h5 className="name">Edward L.</h5>
                        <div className="ratings">
                          <i className="fa fa-star"></i>
                          <i className="fa fa-star"></i>
                          <i className="fa fa-star"></i>
                          <i className="fa fa-star"></i>
                          <i className="fa fa-star"></i>
                          <span>5.00</span>
                        </div>
                        <p><span className="place">France</span><span className="date">15th June, 2018</span></p>
                      </div>
                    </div>
                    <div className="testimonial-slide-body">
                      <p>I've played with Sorteo for several years and really appreciate the site. All of my wins have been credited to my account. Thanks to the entire team at  Sorteo!"</p>
                    </div>
                  </div>
                </div>
                <div className="swiper-slide">
                  <div className="testimonial-slide">
                    <div className="testimonial-slide-header d-flex">
                      <div className="client-thumb">
                        <img src="assets/images/testimonial/1.png"/>
                      </div>
                      <div className="client-details">
                        <h5 className="name">Albert G.</h5>
                        <div className="ratings">
                          <i className="fa fa-star"></i>
                          <i className="fa fa-star"></i>
                          <i className="fa fa-star"></i>
                          <i className="fa fa-star"></i>
                          <i className="fa fa-star"></i>
                          <span>5.00</span>
                        </div>
                        <p><span className="place">France</span><span className="date">15th June, 2018</span></p>
                      </div>
                    </div>
                    <div className="testimonial-slide-body">
                      <p>I've played with Sorteo for several years and really appreciate the site. All of my wins have been credited to my account. Thanks to the entire team at  Sorteo!"</p>
                    </div>
                  </div>
                </div>
                <div className="swiper-slide">
                  <div className="testimonial-slide">
                    <div className="testimonial-slide-header d-flex">
                      <div className="client-thumb">
                        <img src="assets/images/testimonial/2.png"/>
                      </div>
                      <div className="client-details">
                        <h5 className="name">Edward L.</h5>
                        <div className="ratings">
                          <i className="fa fa-star"></i>
                          <i className="fa fa-star"></i>
                          <i className="fa fa-star"></i>
                          <i className="fa fa-star"></i>
                          <i className="fa fa-star"></i>
                          <span>5.00</span>
                        </div>
                        <p><span className="place">France</span><span className="date">15th June, 2018</span></p>
                      </div>
                    </div>
                    <div className="testimonial-slide-body">
                      <p>I've played with Sorteo for several years and really appreciate the site. All of my wins have been credited to my account. Thanks to the entire team at  Sorteo!"</p>
                    </div>
                  </div>
                </div>
                <div className="swiper-slide">
                  <div className="testimonial-slide">
                    <div className="testimonial-slide-header d-flex">
                      <div className="client-thumb">
                        <img src="assets/images/testimonial/1.png"/>
                      </div>
                      <div className="client-details">
                        <h5 className="name">Albert G.</h5>
                        <div className="ratings">
                          <i className="fa fa-star"></i>
                          <i className="fa fa-star"></i>
                          <i className="fa fa-star"></i>
                          <i className="fa fa-star"></i>
                          <i className="fa fa-star"></i>
                          <span>5.00</span>
                        </div>
                        <p><span className="place">France</span><span className="date">15th June, 2018</span></p>
                      </div>
                    </div>
                    <div className="testimonial-slide-body">
                      <p>I've played with Sorteo for several years and really appreciate the site. All of my wins have been credited to my account. Thanks to the entire team at  Sorteo!"</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    );
}
export default Testimonial;