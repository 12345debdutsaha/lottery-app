import React from 'react'

const UsersAroundWorld=()=>{
    return(
        <section className="active-user-section section-padding section-bg border-top">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-lg-8">
            <div className="section-header text-center">
              <h2 className="section-title">Our Users Around The World</h2>
              <p>Over the years we have provided millions of players with tickets to lotteries across the globe and enjoyed having more than one million winners</p>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12">
            <div id="vmap">
              <div className="position-1">
                <span className="dot"></span>
                <div className="details">
                  <h6 className="name">Gaylen Hylen</h6>
                  <p className="area">From: Argentina</p>
                </div>
              </div>
              <div className="position-2">
                <span className="dot"></span>
                <div className="details">
                  <h6 className="name">Gaylen Hylen</h6>
                  <p className="area">From: Argentina</p>
                </div>
              </div>
              <div className="position-3">
                <span className="dot"></span>
                <div className="details">
                  <h6 className="name">Gaylen Hylen</h6>
                  <p className="area">From: Argentina</p>
                </div>
              </div>
              <div className="position-4">
                <span className="dot"></span>
                <div className="details">
                  <h6 className="name">Gaylen Hylen</h6>
                  <p className="area">From: Argentina</p>
                </div>
              </div>
              <div className="position-5">
                <span className="dot"></span>
                <div className="details">
                  <h6 className="name">Gaylen Hylen</h6>
                  <p className="area">From: Argentina</p>
                </div>
              </div>
              <div className="position-6">
                <span className="dot"></span>
                <div className="details">
                  <h6 className="name">Gaylen Hylen</h6>
                  <p className="area">From: Argentina</p>
                </div>
              </div>
              <div className="position-7">
                <span className="dot"></span>
                <div className="details">
                  <h6 className="name">Gaylen Hylen</h6>
                  <p className="area">From: Argentina</p>
                </div>
              </div>
              <div className="position-8">
                <span className="dot"></span>
                <div className="details">
                  <h6 className="name">Gaylen Hylen</h6>
                  <p className="area">From: Argentina</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-3 col-md-3 col-sm-6 col-6">
            <div className="active-item text-center">
              <span className="amount">3 M</span>
              <p>Registered players</p>
            </div>
          </div>
          <div className="col-lg-3 col-md-3 col-sm-6 col-6">
            <div className="active-item text-center">
              <span className="amount">€ 1.9 BN</span>
              <p>Total in placed bets</p>
            </div>
          </div>
          <div className="col-lg-3 col-md-3 col-sm-6 col-6">
            <div className="active-item text-center">
              <span className="amount">€ 550 M</span>
              <p>Total in payouts</p>
            </div>
          </div>
          <div className="col-lg-3 col-md-3 col-sm-6 col-6">
            <div className="active-item text-center">
              <span className="amount">€ 44 M</span>
              <p>Biggest ever pay-out</p>
            </div>
          </div>
          <div className="col-lg-12 text-center">
            <a href="#0" className="cmn-btn">join with us</a>
          </div>
        </div>
      </div>
    </section>
    );
}

export default UsersAroundWorld;