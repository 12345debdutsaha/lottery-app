import React from 'react'

const ChooseUs=()=>{
    return(
        <section className="choose-us-section section-padding">
      <div className="choose-us-image"><img src="assets/images/elements/mouse.png" /></div>
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-lg-6">
            <div className="section-header text-center">
              <h2 className="section-title">Why Choose Us?</h2>
              <p>Sorteo makes playing the world's largest lotteries easy and fun.</p>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-9">
            <div className="row mt-mb-15">
              <div className="col-lg-4 col-sm-6">
                <div className="choose-item text-center">
                  <div className="front">
                    <div className="icon">
                      <img src="assets/images/svg-icons/choose-us-icons/1.svg" />
                    </div>
                    <h4 className="title">Biggest lottery jackpots</h4>
                  </div>
                  <div className="back">
                    <p>One of the core advantages of playing an online lotto is that it is both safe and secure. So, there is no need for you to queue, or visit a retail store, to get tickets.</p>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-sm-6">
                <div className="choose-item text-center">
                  <div className="front">
                    <div className="icon">
                      <img src="assets/images/svg-icons/choose-us-icons/2.svg"/>
                    </div>
                    <h4 className="title">No commission on Winnings</h4>
                  </div>
                  <div className="back">
                    <p>One of the core advantages of playing an online lotto is that it is both safe and secure. So, there is no need for you to queue, or visit a retail store, to get tickets.</p>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-sm-6">
                <div className="choose-item text-center">
                  <div className="front">
                    <div className="icon">
                      <img src="assets/images/svg-icons/choose-us-icons/3.svg" />
                    </div>
                    <h4 className="title"> Safe and Secure Playing</h4>
                  </div>
                  <div className="back">
                    <p>One of the core advantages of playing an online lotto is that it is both safe and secure. So, there is no need for you to queue, or visit a retail store, to get tickets.</p>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-sm-6">
                <div className="choose-item text-center">
                  <div className="front">
                    <div className="icon">
                      <img src="assets/images/svg-icons/choose-us-icons/4.svg" />
                    </div>
                    <h4 className="title">Instant payout system</h4>
                  </div>
                  <div className="back">
                    <p>One of the core advantages of playing an online lotto is that it is both safe and secure. So, there is no need for you to queue, or visit a retail store, to get tickets.</p>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-sm-6">
                <div className="choose-item text-center">
                  <div className="front">
                    <div className="icon">
                      <img src="assets/images/svg-icons/choose-us-icons/5.svg"/>
                    </div>
                    <h4 className="title">Performance Bonuses</h4>
                  </div>
                  <div className="back">
                    <p>One of the core advantages of playing an online lotto is that it is both safe and secure. So, there is no need for you to queue, or visit a retail store, to get tickets.</p>
                  </div>
                </div>
              </div>
              <div className="col-lg-4 col-sm-6">
                <div className="choose-item text-center">
                  <div className="front">
                    <div className="icon">
                      <img src="assets/images/svg-icons/choose-us-icons/6.svg" />
                    </div>
                    <h4 className="title">Dedicated Support</h4>
                  </div>
                  <div className="back">
                    <p>One of the core advantages of playing an online lotto is that it is both safe and secure. So, there is no need for you to queue, or visit a retail store, to get tickets.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    );
}

export default ChooseUs;