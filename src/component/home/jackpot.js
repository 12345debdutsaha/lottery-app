import React from 'react'

const JackPot=()=>{
    return(
        <section className="jackpot-section section-padding">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-lg-5">
            <div className="section-header text-center">
              <h2 className="section-title">Lottery Jackpots</h2>
              <p>Choose from the Powerball, Mega Millions, Lotto or Lucky Day Lotto and try for a chance to win a big cash prize</p>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-4 col-md-6">
            <div className="jackpot-item text-center">
              <img src="assets/images/elements/jackpot-1.png" />
              <span className="amount">€161,557,581</span>
              <h5 className="title">US Powerball</h5>
              <p className="next-draw-time">Next Draw : <span id="remainTime1"></span></p>
              <a href="#0" className="cmn-btn">play now!</a>
            </div>
          </div>
          <div className="col-lg-4 col-md-6">
            <div className="jackpot-item text-center">
              <img src="assets/images/elements/jackpot-2.png" />
              <span className="amount">€161,557,581</span>
              <h5 className="title">Cancer Charity</h5>
              <p className="next-draw-time">Next Draw : <span id="remainTime2"></span></p>
              <a href="#0" className="cmn-btn">play now!</a>
            </div>
          </div>
          <div className="col-lg-4 col-md-6">
            <div className="jackpot-item text-center">
              <img src="assets/images/elements/jackpot-3.png"/>
              <span className="amount">€161,557,581</span>
              <h5 className="title">EuroJackpot</h5>
              <p className="next-draw-time">Next Draw : <span id="remainTime3"></span></p>
              <a href="#0" className="cmn-btn">play now!</a>
            </div>
          </div>
          <div className="col-lg-12 text-center">
            <a href="#0" className="text-btn">Show all lotteries</a>
          </div>
        </div>
      </div>
    </section>
    );
}
export default JackPot;