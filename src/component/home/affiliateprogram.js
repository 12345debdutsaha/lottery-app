import React from 'react'

const AffiliatedProgram=()=>{
    return(
        <section className="affiliate-section section-padding">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-lg-8">
            <div className="section-header text-center">
              <h2 className="section-title">Affiliate Programs</h2>
              <p>Tell A Friend is our unique lottery friends promotion club that enables you to earn amazing bonus money rewards for inviting friends to play the world’s biggest jackpots!</p>
            </div>
          </div>
        </div>
        <div className="row m-bottom-not-30">
          <div className="col-lg-3 col-sm-6">
            <div className="affiliate-item text-center">
              <div className="icon">
                <div className="icon-inner">
                  <img src="assets/images/svg-icons/affiliate-icons/1.svg" />
                </div>
              </div>
              <div className="content">
                <h4 className="title">High Revenues</h4>
                <p>We offer the best commissions in the market and provide proven.</p>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-sm-6">
            <div className="affiliate-item text-center">
              <div className="icon">
                <div className="icon-inner">
                  <img src="assets/images/svg-icons/affiliate-icons/4.svg"/>
                </div>
              </div>
              <div className="content">
                <h4 className="title">Reliable Payments</h4>
                <p>Payments are made monthly via a variety of  payment methods.</p>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-sm-6">
            <div className="affiliate-item text-center">
              <div className="icon">
                <div className="icon-inner">
                  <img src="assets/images/svg-icons/affiliate-icons/2.svg" />
                </div>
              </div>
              <div className="content">
                <h4 className="title">Unlimited Affiliates</h4>
                <p>Thee is no limit  for your number of affiliates and no earning limit.</p>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-sm-6">
            <div className="affiliate-item text-center">
              <div className="icon">
                <div className="icon-inner">
                  <img src="assets/images/svg-icons/affiliate-icons/3.svg"/>
                </div>
              </div>
              <div className="content">
                <h4 className="title">Dedicated Support</h4>
                <p>Our dedicated technical support team works with you to understand and identify</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    );
}
export default AffiliatedProgram;