import React from 'react'

const HowToPlay=()=>{
    return(
    <section className="work-steps-section section-padding border-top">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-lg-7">
            <div className="section-header text-center">
              <h2 className="section-title">how it works</h2>
              <p>Sorteo is the best way to play these exciting lotteries from anywhere in the world.</p>
            </div>
          </div>
        </div>
        <div className="row align-items-center">
          <div className="col-lg-6">
            <div className="work-steps-items-part d-flex">
              <div className="line"><img src="assets/images/elements/line.png"/></div>
              <div className="work-steps-item">
                <div className="work-steps-item-inner">
                  <div className="icon"><img src="assets/images/svg-icons/how-work-icons/1.svg"/>
                    <span className="count-num">01</span></div>
                  <h4 className="title">choose</h4>
                  <p>Choose your lottery & pick your numbers</p>
                </div>
              </div>
              <div className="work-steps-item">
                <div className="work-steps-item-inner">
                  <div className="icon"><img src="assets/images/svg-icons/how-work-icons/2.svg"/>
                    <span className="count-num">02</span></div>
                  <h4 className="title">buy</h4>
                  <p>Complete your purchase</p>
                </div>
              </div>
              <div className="work-steps-item">
                <div className="work-steps-item-inner">
                  <div className="icon"><img src="assets/images/svg-icons/how-work-icons/3.svg"/>
                    <span className="count-num">01</span></div>
                  <h4 className="title">win</h4>
                  <p>Start dreaming, you're almost there</p>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-6">
            <div className="work-steps-thumb-part">
              <img src="assets/images/elements/step.png"/>
              <a href="https://www.youtube.com/embed/aFYlAzQHnY4" data-rel="lightcase:myCollection" className="play-btn"><i className="fa fa-play"></i></a>
            </div>
          </div>
        </div>
      </div>
    </section>
    );
}

export default HowToPlay;