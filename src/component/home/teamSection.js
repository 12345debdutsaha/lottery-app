import React from 'react'

const TeamSection=()=>{
    return(
        <section className="team-section section-padding section-bg border-top">
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-lg-8">
            <div className="section-header text-center">
              <h2 className="section-title">Our Management Team</h2>
              <p>Our team of creative programmers, marketing experts, and members of the global lottery community have worked together to build the ultimate lottery site, and every win and happy customer reminds us how lucky we are to be doing what we love.</p>
            </div>
          </div>
        </div>
        <div className="row m-bottom-not-30">
          <div className="col-lg-3 col-sm-6">
            <div className="team-single text-center">
              <div className="thumb">
                <img src="assets/images/team/1.png"/>
                <ul className="team-social-link d-flex justify-content-center">
                  <li><a href="#"><i className="fa fa-facebook-f"></i></a></li>
                  <li><a href="#"><i className="fa fa-twitter"></i></a></li>
                  <li><a href="#"><i className="fa fa-google-plus"></i></a></li>
                  <li><a href="#"><i className="fa fa-instagram"></i></a></li>
                </ul>
              </div>
              <div className="content">
                <h3 className="name">Philip Brower</h3>
                <span className="designation">Co-Founder & CEO</span>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-sm-6">
            <div className="team-single text-center">
              <div className="thumb">
                <img src="assets/images/team/2.png"/>
                <ul className="team-social-link d-flex justify-content-center">
                  <li><a href="#"><i className="fa fa-facebook-f"></i></a></li>
                  <li><a href="#"><i className="fa fa-twitter"></i></a></li>
                  <li><a href="#"><i className="fa fa-google-plus"></i></a></li>
                  <li><a href="#"><i className="fa fa-instagram"></i></a></li>
                </ul>
              </div>
              <div className="content">
                <h3 className="name">Penny Tool</h3>
                <span className="designation">IT Specialist</span>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-sm-6">
            <div className="team-single text-center">
              <div className="thumb">
                <img src="assets/images/team/3.png"/>
                <ul className="team-social-link d-flex justify-content-center">
                  <li><a href="#"><i className="fa fa-facebook-f"></i></a></li>
                  <li><a href="#"><i className="fa fa-twitter"></i></a></li>
                  <li><a href="#"><i className="fa fa-google-plus"></i></a></li>
                  <li><a href="#"><i className="fa fa-instagram"></i></a></li>
                </ul>
              </div>
              <div className="content">
                <h3 className="name">Conrad Berry</h3>
                <span className="designation">Consultant</span>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-sm-6">
            <div className="team-single text-center">
              <div className="thumb">
                <img src="assets/images/team/4.png"/>
                <ul className="team-social-link d-flex justify-content-center">
                  <li><a href="#"><i className="fa fa-facebook-f"></i></a></li>
                  <li><a href="#"><i className="fa fa-twitter"></i></a></li>
                  <li><a href="#"><i className="fa fa-google-plus"></i></a></li>
                  <li><a href="#"><i className="fa fa-instagram"></i></a></li>
                </ul>
              </div>
              <div className="content">
                <h3 className="name">Alexis Brady</h3>
                <span className="designation">Financial Adviser</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    );
}

export default TeamSection;