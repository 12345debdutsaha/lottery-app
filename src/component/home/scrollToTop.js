import React from 'react'

const ScrollToTop=()=>{
    return(
        <div className="scroll-to-top">
        <span className="scroll-icon">
          <i className="fa fa-angle-up"></i>
        </span>
      </div>
    );
}

export default ScrollToTop;