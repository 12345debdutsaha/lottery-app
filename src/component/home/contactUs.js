import React from 'react'

const ContactUs=()=>{
    return(
        <section className="contact-section border-top overflow-hidden has_bg_image">
      <div className="container">
        <div className="row justify-content-between">
          <div className="col-lg-6">
            <div className="section-header text-center">
              <h5 className="top-title">We answer all of your questions</h5>
              <h2 className="section-title text-uppercase">Contact us</h2>
              <p>If you have any questions or queries our helpful support team will be more than happy to assist.</p>
            </div>
            <div className="contact-form-area">
              <form className="contact-form">
                <div className="form-grp">
                  <input type="text" name="contact_name" id="contact_name" placeholder="Full Name"/>
                </div>
                <div className="form-grp">
                  <input type="email" name="contact_email" id="contact_email" placeholder="Email Address"/>
                </div>
                <div className="form-grp">
                  <input type="tel" name="contact_phone" id="contact_phone" placeholder="Phone No"/>
                </div>
                <div className="form-grp">
                  <textarea name="contact_message" id="contact_message" placeholder="Message"></textarea>
                </div>
                <div className="form-grp">
                  <input className="submit-btn" type="submit" value="sent message"/>
                </div>
              </form>
            </div>
          </div>
          <div className="col-lg-5">
            <div className="contact-thumb">
              <img src="assets/images/elements/contact.png" />
            </div>
          </div>
        </div>
      </div>
    </section>
    );
}

export default ContactUs;