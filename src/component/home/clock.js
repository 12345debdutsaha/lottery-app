import React from 'react'

const Clock=()=>{
    return(
    <section className="lottery-timer-section">
      <div className="container">
        <div className="row justify-content-between">
          <div className="col-xl-5">
            <div className="timer-content">
              <h3 className="title">Buy Lottery Tickets Online</h3>
              <p>Buy lottery tickets online to the biggest lotteries in the world offering huge jackpot prizes that you can win when you play online lottery.</p>
            </div>
          </div>
          <div className="col-xl-5 text-center">
            <div className="timer-part">
              <div className="clock"></div>
            </div>
          </div>
          <div className="col-xl-2">
            <div className="link-area text-center">
              <a href="#0" className="border-btn">register & play</a>
              <a href="#0" className="text-btn">view all offer</a>
            </div>
          </div>
        </div>
      </div>
    </section>

    );
}

export default Clock;