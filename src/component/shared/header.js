import React from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink
 } from 'reactstrap';

  import {Link} from 'react-router-dom'

export default class Header extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <div>
        <Navbar color="light" light expand="md">
          <NavbarBrand><Link to="/">Lottery</Link></NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink><Link to="/about">ABOUT US</Link></NavLink>
              </NavItem>
              <NavItem>
                <NavLink><Link to="/latestwinner">LASTEST WINNER</Link></NavLink>
              </NavItem>
              <NavItem>
                <NavLink><Link to="/prevresult">PREVIOUS RESULT</Link></NavLink>
              </NavItem>
              <NavItem>
                <NavLink><Link to="/faq">FAQ</Link></NavLink>
              </NavItem>
              <NavItem>
                <NavLink><Link to="/contactus">CONTACT US</Link></NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}
