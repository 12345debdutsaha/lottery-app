import React from 'react'

const Footer=()=>{
    return(
        <footer className="footer-section">
      <div className="footer-top border-top border-bottom has_bg_image" >
        <div className="footer-top-first">
          <div className="container">
            <div className="row">
              <div className="col-lg-6 col-md-5 col-sm-4 text-center text-sm-left">
                <a href="home-one.html" className="site-logo"><img src="assets/images/logo.png"/></a>
              </div>
              <div className="col-lg-6 col-md-7 col-sm-8">
                <div className="number-count-part d-flex">
                  <div className="number-count-item">
                    <span className="number">2,3402,233</span>
                    <p>TOTAL MEMBERS</p>
                  </div>
                  <div className="number-count-item">
                    <span className="number">1,9402,575</span>
                    <p>TOTAL winner</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="footer-top-second">
          <div className="container">
            <div className="row justify-content-between">
              <div className="col-xl-2 col-lg-3 col-md-3 col-sm-6">
                <div className="footer-widget widget-about">
                  <h3 className="widget-title">About Sorteo</h3>
                  <ul className="footer-list-menu">
                    <li><a href="#0">About us</a></li>
                    <li><a href="#0">How it Works</a></li>
                    <li><a href="#0">Our services</a></li>
                    <li><a href="#0">Blog</a></li>
                    <li><a href="#0">Contact us</a></li>
                  </ul>
                </div>
              </div>
              <div className="col-xl-2 col-lg-3 col-md-3 col-sm-6">
                <div className="footer-widget widget-links">
                  <h3 className="widget-title">Quick links</h3>
                  <ul className="footer-list-menu">
                    <li><a href="#0">My Account</a></li>
                    <li><a href="#0">Affiliate Program</a></li>
                    <li><a href="#0">Terms & Conditions</a></li>
                    <li><a href="#0">Privacy Policy</a></li>
                    <li><a href="#0">Sorteo Licenses</a></li>
                  </ul>
                </div>
              </div>
              <div className="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                <div className="footer-widget widget-subscribe">
                  <h3 className="widget-title">email newsletters</h3>
                  <div className="subscribe-part">
                    <p>Subscribe now and receive weekly newsletter for latest draw and offer news and much more!</p>
                    <form className="subscribe-form">
                      <input type="email" name="subs_email" id="subs_email" placeholder="Email address"/>
                      <input type="submit" value="subscribe"/>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="footer-bottom">
        <div className="container">
          <div className="row justify-content-between align-items-center">
            <div className="col-lg-6 col-sm-7">
              <div className="copy-right-text">
                <p>© 2019 <a href="#">Sorteo</a> - All Rights Reserved.</p>
              </div>
            </div>
            <div className="col-lg-6 col-sm-5">
              <ul className="footer-social-links d-flex justify-content-end">
                <li><a href="#0"><i className="fa fa-facebook"></i></a></li>
                <li><a href="#0"><i className="fa fa-twitter"></i></a></li>
                <li><a href="#0"><i className="fa fa-google-plus"></i></a></li>
                <li><a href="#0"><i className="fa fa-instagram"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </footer>
    );
}
export default Footer;