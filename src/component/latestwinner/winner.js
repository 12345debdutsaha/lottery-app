import React from 'react'

const Winner=()=>{
    return(
        <div className="col-lg-3 col-md-4 col-sm-6">
          <div className="winner-item m-bottom-30">
            <div className="thumb">
              <img src="assets/images/winner/1.jpg" />
            </div>
            <div className="content">
              <span className="amount">$80,000</span>
              <span className="game-name">powerball</span>
              <span className="name">Willie Garcia</span>
            </div>
          </div>
        </div>
    );
}

export default Winner;