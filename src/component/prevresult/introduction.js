import React from 'react'

const Introduction=()=>{
    return(
        <section className="inner-page-banner has_bg_image">
        <div className="container">
        <div className="row">
            <div className="col-lg-12">
            <div className="inner-page-banner-area">
                <h1 className="page-title">Latest Lottery Results</h1>
                <nav aria-label="breadcrumb" className="page-header-breadcrumb">
                <ol className="breadcrumb">
                    <li className="breadcrumb-item"><a href="home-one.html">Home</a></li>
                    <li className="breadcrumb-item">result</li>
                </ol>
                </nav>
            </div>
            </div>
        </div>
        </div>
    </section>
    );
}

export default Introduction;