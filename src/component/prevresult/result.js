import React from 'react'

const Result=(props)=>{
    return(
        <section className="lottery-result-section section-padding">
    <div className="container">
      {!(props.tittle==="alllotteries")&&<div className="row justify-content-center">
        <div className="col-lg-8">
          <div className="section-header text-center">
            <h2 className="section-title">Latest Lottery results</h2>
            <p>Check the current lotto results and to see if you have won! All of the results presented on our website are the official results of the last draw of a particular lottery. We advise you to check lotto numbers from your ticket and compare them with the numbers drawn in a particular lottery draw.</p>
          </div>
        </div>
      </div>}
      <div className="row mt-mb-15">
        <div className="col-lg-6">
          <div className="latest-result-item">
            <div className="latest-result-item-header d-flex align-items-center justify-content-between">
              <div className="icon"><img src="assets/images/elements/jackpot-1.png"/></div>
              {(props.tittle==="alllotteries")&&<div className="btn-area">
                <a href="#0" className="cmn-btn">play now !</a>
              </div>}
            </div>
            <div className="latest-result-item-body d-flex align-items-center justify-content-between">
              <div className="winner-num">
                <span className="winner-num-title">winner numbers</span>
                <ul className="number-list">
                  <li>19</li>
                  <li>31</li>
                  <li>21</li>
                  <li>19</li>
                  <li className="active">69</li>
                </ul>
              </div>
              <div className="next-jack-amount">
                <span>Next estimated jackpot</span>
                <span className="amount">US  541,000,000</span>
              </div>
            </div>
            <div className="latest-result-item-footer">
              {(props.tittle==="alllotteries")&&<div className="text-center"><i className="fa fa-hourglass-half"></i>3 days left</div>}
            </div>
          </div>
        </div>
        </div>
        <div className="col-lg-12 text-center">
          <button type="button" className="cmn-btn">load more</button>
        </div>
      </div>
  </section>
    );
}
export default Result;