import React from 'react'

const Faq=()=>{
    return(
<section className="question-section border-top section-padding section-bg">
    <div className="container">
      <div className="row justify-content-between">
        <div className="col-lg-4">
          <div className="thumb text-lg-right text-center">
            <img src="assets/images/elements/faq.png"/>
          </div>
        </div>
        <div className="col-lg-7">
          <div className="content">
            <h2 className="title">If you have any questions</h2>
            <p>Our top priorities are to protect your privacy, provide secure transactions, and safeguard your data. When you're ready to play, registering an account is required so we know you're of legal age and so no one else can use your account.We answer the most commonly asked lotto questions..</p>
            <a href="#0" className="cmn-btn">Check FAQs</a>
          </div>
        </div>
      </div>
    </div>
  </section>
    );
}

export default Faq;