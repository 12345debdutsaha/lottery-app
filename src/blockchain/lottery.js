import web3 from '../web3'
import {getWeb3Instance} from '../contracts/AllLottery';
import {NewLottery} from '../actions/index';

export const deployContract=async()=>{
    const AllLottery=await getWeb3Instance()
    const data=await NewLottery()
    const userAddress=await web3.eth.getAccounts()
    await AllLottery.methods.newLottery(data.address).send({
        from:userAddress[0]
    })
    console.log("Deployed a contract")
}