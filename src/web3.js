import Web3 from 'web3';

let web3;
const Web3Setup=async()=>{
if (window.ethereum) {
    web3 = new Web3(window.ethereum);
    try {
        // Request account access if needed
        await window.ethereum.enable();
    } catch (error) {
        // User denied account access...
    }
}
// Legacy dapp browsers...
else if (window.web3) {
    web3 = new Web3(window.web3.currentProvider);
}
// Non-dapp browsers...
else {
    alert('Non-Ethereum browser detected. You should consider trying MetaMask!');
}
}
Web3Setup();
export default web3;