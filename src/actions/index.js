import Axios from 'axios'
import url from '../url'
export const NewLottery=async()=>{
    let response=await Axios.get(url+'deploy/Lottery').then(response=>{
        if(response.status==200)
        {
            return response.data;
        }
        return null;
    }).catch(err=>{
        return null
    })
    return response;
}

export const fileUpload=async(file)=>{
    return await Axios.post(url+"fileupload",{file:file}).then(res=>{
        if(res.status===200)
        {
            return res.data
        }else{
            return null
        }
    }).catch(err=>{
        return null
    })
}