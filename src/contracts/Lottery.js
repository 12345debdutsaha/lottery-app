import web3 from '../web3';
import Axios from 'axios';
import url from '../url';

const fetchAbi=async()=>{
    let res=await Axios.get(url+"abi/Lottery")
    if(res.status===200)
    {
        return res.data.abi
    }else{
        return null
    }
}
export const getWeb3Instance=async(address)=>{
    const abi=await fetchAbi();
    if(abi!==null)
    return new web3.eth.Contract(JSON.parse(abi),address);
    else
    return null
}



