import web3 from '../web3';
import Axios from 'axios';
import url from '../url';

const address='0xEaa93B59ab7592F023fC64788bbA1CC102eeFc3F'
const fetchAbi=async()=>{
    let res=await Axios.get(url+"abi/AllLottery")
    if(res.status===200)
    {
        return res.data.abi
    }else{
        return null
    }
}
export const getWeb3Instance=async()=>{
    const abi=await fetchAbi();
    if(abi!==null)
    return new web3.eth.Contract(JSON.parse(abi),address);
    else
    return null
}



