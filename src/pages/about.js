import React from 'react';
import ChooseUs from '../component/home/chooseus';
import Testimonial from '../component/home/testimonial';
import TeamSection from '../component/home/teamSection';
import Footer from '../component/shared/footer';
import PreLoader from '../component/shared/preloader';
import Header from '../component/shared/header';
import ScrollToTop from '../component/home/scrollToTop';
const About=(props)=>{
    return(
<div>
    <Header/>
    <section className="about-section section-padding">
    <div className="container-fluid">
      <div className="row justify-content-between">
        <div className="col-xl-5 p-xl-0">
          <div className="thumb">
            <img src="assets/images/about-image.jpg"/>
          </div>
        </div>
        <div className="col-xl-6 p-xl-0">
          <div className="content">
            <h2 className="title">A FEW WORDS ABOUT US</h2>
            <p>Our goal is to provide our customers with the most convenient and enjoyable lottery purchasing experience.</p>
            <p>It’s not just what we do, it’s who we are. From past to present, we are a success story still in the making.With an enthusiasm to serve, our commitment to providing fun and fair games is matched by our desire to make a positive difference in the community.</p>
            <ul className="cmn-list">
              <li>Playing It Global for Over a Decade</li>
              <li>Play from Anywhere in the World</li>
              <li>Absolutely No Commissions Taken</li>
              <li>A Simple and Secure Service</li>
              <li>Winning Made Easy</li>
            </ul>
            <a href="#0" className="cmn-btn">a bit of history</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <ChooseUs/>
  <Testimonial/>
  <TeamSection/>
  <Footer/>
  <div>
      <ScrollToTop/>
  </div>
</div>
    );
}

export default About;