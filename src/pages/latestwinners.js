import React from 'react'
import Winner from '../component/latestwinner/winner';
import Header from '../component/shared/header';
import Footer from '../component/shared/footer';
import ScrollToTop from '../component/home/scrollToTop';

const LatestWinner=()=>{
    return(
    <div>
    <Header/>
    <section className="latest-winner-section section-padding">
    <div className="container">
      <div className="row justify-content-center">
        <div className="col-lg-8">
          <div className="section-header text-center">
            <h2 className="section-title">List of Lottery Winners</h2>
            <p>Our biggest winners have won lottery jackpots and million-dollar prizes - read their stories below. Perhaps your lottery win will soon be added to our list!</p>
          </div>
        </div>
      </div>
      <div className="row m-bottom-not-30">
        <Winner/>
      </div>
    </div>
  </section>
  <Footer/>
  <div>
      <ScrollToTop/>
  </div>
  </div>
    );
}
export default LatestWinner;