import React from 'react';
import Faq from '../component/prevresult/faq';
import Result from '../component/prevresult/result';
import Footer from '../component/shared/footer';
import Header from '../component/shared/header';
import Heading from '../component/shared/heading';


const PreviousResult=(props)=>{
    return(
    <div>
        <Header/>
        <Heading tittle="Latest Result"/>
        <Result/>
        <Faq/>
        <Footer/>
    </div>
    );
}

export default PreviousResult;