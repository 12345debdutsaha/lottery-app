import React from 'react'
import Header from '../component/shared/header';
import Heading from '../component/shared/heading';
import Result from '../component/prevresult/result';
import Faq from '../component/prevresult/faq';
import Footer from '../component/shared/footer';
import CreateButton from '../component/AllLotteries/createButton'
const AllLotteries=()=>{
    return(
        <div>
            <Header/>
            <Heading tittle="AllLotteries"/>
            <CreateButton/>
            <Result tittle="alllotteries"/>
            <Faq/>
            <Footer/>
        </div>
    );
}

export default AllLotteries;