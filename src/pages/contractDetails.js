import React from 'react';
import Header from '../component/shared/header';
import Players from '../component/contractdetails/players';
import {Row,Col,Container} from 'reactstrap'

const ContractDetails=(props)=>{
    return(
        <div>
            <Header/>
            <Row>
                <Col md="8">
                    <h3 style={{marginLeft:10}}>PLAYERS IN THIS LOTTERY</h3>
                    <Container style={{display:"flex",flexDirection:"row"}}>
                        <Players/>
                        <Players/>
                        <Players/>
                        <Players/>
                    </Container>
                </Col>
            </Row>
        </div>
    );
}

export default ContractDetails;