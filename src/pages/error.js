import React from 'react'
import Header from '../component/shared/header';
import Footer from '../component/shared/footer';
import Heading from '../component/shared/heading';

const Error404=()=>{
    return(
        <div>
        <Header/>
        <Heading tittle="Error"/>
    <section class="error-section section-padding">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <div class="error-content text-center">
            <img src="assets/images/elements/404.png"/>
            <h3 class="title">Oops... It looks  like you ‘re lost ! </h3>
            <p>The page you were looking for dosen’t exist.</p>
            <a href="home-one.html" class="cmn-btn">GO BACK</a>
          </div>
        </div>
      </div>
    </div>
  </section>
  <Footer/>
        </div>
    );
}

export default Error404;