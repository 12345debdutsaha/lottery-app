import React from 'react';
import Header from '../component/shared/header'
import Introduction from '../component/home/introduction'
import Clock from '../component/home/clock'
import JackPot from '../component/home/jackpot'
import LotteryResult from '../component/home/lotteryResult';
import ChooseUs from '../component/home/chooseus';
import HowToPlay from '../component/home/howtoplay';
import BuyTicketsOnline from '../component/home/buyticketsonline'
import AffiliatedProgram from '../component/home/affiliateprogram';
import PaymentMethods from '../component/home/paymentMethod';
import UsersAroundWorld from '../component/home/usersAroundWorld';
import Testimonial from '../component/home/testimonial';
import TeamSection from '../component/home/teamSection';
import ContactUs from '../component/home/contactUs';
import Footer from '../component/shared/footer';
import ScrollToTop from '../component/home/scrollToTop';
const Index=(props)=>{
    return(
<div>
<div>
  <Header/> 
    <Introduction/>
    <Clock/>
    <JackPot/>
    <LotteryResult/>
    <ChooseUs/>
    <HowToPlay/>
    <BuyTicketsOnline/>
    <AffiliatedProgram/>
    <PaymentMethods/>
    <UsersAroundWorld/>
    <Testimonial/>
    <TeamSection/>
    <ContactUs/>
    <Footer/>
  </div>
  <ScrollToTop/>
  </div>
);
}

export default Index;