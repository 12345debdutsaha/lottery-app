import React from 'react'
import Header from '../component/shared/header';
import Footer from '../component/shared/footer';
import ScrollToTop from '../component/home/scrollToTop';
import Heading from '../component/shared/heading';
const ContactUs=()=>{
    return(
        <div>
        <Header/>
        <Heading tittle="Contact us"/>
    <section className="contact-section">
    <div className="container">
      <div className="row">
        <div className="col-lg-12">
          <div className="contact-wrapper">
            <div className="contact-area d-flex">
              <div className="contact-form-area">
                <h3 className="title">Get In Touch</h3>
                <p>If you have any questions or queries our helpful support team will be more than happy to assist.</p>
                <form className="contact-form">
                  <div className="form-grp">
                    <input type="text" name="contact_name" id="contact_name" placeholder="Full Name"/>
                  </div>
                  <div className="form-grp">
                    <input type="email" name="contact_email" id="contact_email" placeholder="Email Address"/>
                  </div>
                  <div className="form-grp">
                    <input type="tel" name="contact_phone" id="contact_phone" placeholder="Phone No"/>
                  </div>
                  <div className="form-grp">
                    <textarea name="contact_message" id="contact_message" placeholder="Message"></textarea>
                  </div>
                  <div className="form-grp">
                    <input className="submit-btn" type="submit" value="sent message"/>
                  </div>
                </form>
              </div>
              <div className="address-area has_bg_image">
                <div className="address-area-header">
                  <h3 className="title">We Are Available</h3>
                  <span>24 Hours A Day, 365 Days A Year</span>
                </div>
                <div className="contact-address">
                  <h3 className="title">Contact Us</h3>
                  <ul>
                    <li>
                      <div className="icon"><i className="fa fa-phone"></i></div>
                      <div className="content">
                        <p>012 345 6789</p>
                        <p>012 345 6789</p>
                      </div>
                    </li>
                    <li>
                      <div className="icon"><i className="fa fa-envelope-o"></i></div>
                      <div className="content">
                        <p>info@sorteo.com</p>
                        <p>info@sorteo.com</p>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="contact-wrapper-footer">
              <span>Please see our Frequent Asked Questions (FAQ) page to read more information:</span>
              <a href="#0" className="cmn-btn">check FAQs</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div style={{marginTop:200}}>
  <Footer/>
  </div>
  <div>
      <ScrollToTop/>
  </div>
</div>
    );
}
export default ContactUs;